Sutty Panel Playbook
--------------------

Ansible playbook for Sutty CMS.

Inventory
---------

Create, link or clone an `inventory.yml` file with the host roles.

```yaml
---
sutty_panel:
  hosts:
    host.name:
    host2.name:
sutty_nodes:
  hosts:
    node.name:
```

Host and group vars
-------------------

Create, link or clone a directory `host_vars/` with a YAML file for each
host, containing all host-specific variables.

A default `group_vars/` directory exists.  Check CHANGEME variables for
stuff that requires customization.

Credentials
-----------

Auto-generated credentials are stored on `_credentials/` and ignored by
git.
